/*
 * 版权所有 2009-2012山东新北洋信息技术股份有限公司保留所有权利。
 */

package com.easipos.easilocker.snbc.common;

import android.content.ComponentName;
import android.content.Context;
import android.content.Intent;
import android.content.ServiceConnection;
import android.os.IBinder;

import com.easipos.easilocker.bvm.GridAidlInterface;

/**
 * 包名称：com.snbc.bvmtest.common
 * 描述：绑定类
 * 作者：王峰
 * 日期：2019/10/30
 * 公司：山东新北洋信息技术股份有限公司
 */
public class CommonServerBind {
    private boolean isBvmAidlBindServer;
    private static CommonServerBind commonServerBind;
    //绑定stub
    private GridAidlInterface stub;

    //连接aidl
    private  ServiceConnection conn = new ServiceConnection() {
        @Override
        public void onServiceConnected(ComponentName componentName, IBinder iBinder) {
            stub = GridAidlInterface.Stub.asInterface(iBinder);
            isBvmAidlBindServer = true;
        }

        @Override
        public void onServiceDisconnected(ComponentName componentName) {
            isBvmAidlBindServer = false;
        }
    };
    public GridAidlInterface getStub() {
        return stub;
    }
    /**
     * @param
     * @describe：获得对象
     * @return com.snbc.bvmtest.common.CommonServerBind
     * @author 王峰
     **/
    public static CommonServerBind getInstance(){
        if (commonServerBind == null){
            commonServerBind = new CommonServerBind();
        }
        return commonServerBind;
    }

    /**
     * @param context
     * @describe：绑定服务
     * @return void
     * @author 王峰
     **/
    public void bindServer(Context context) {
        if(!isBvmAidlBindServer) {
            final Intent intent = new Intent();
            intent.setAction("android.intent.action.SnbcGridService");
            intent.setPackage("com.snbc.bvm");
            context.bindService(intent, conn, Context.BIND_AUTO_CREATE);
        }

    }
    /**
     * @param context
     * @describe：解绑服务
     * @return void
     * @author 王峰
     **/
    public  void unbindServer(Context context){
        context.unbindService(conn);
    }

    public boolean isBindServer(){
        return isBvmAidlBindServer;
    }
}
