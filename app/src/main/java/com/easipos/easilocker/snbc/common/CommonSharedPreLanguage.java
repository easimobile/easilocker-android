/*
 * 版权所有 2009-2012山东新北洋信息技术股份有限公司保留所有权利。
 */


package com.easipos.easilocker.snbc.common;

import android.content.Context;
import android.content.SharedPreferences;

public class CommonSharedPreLanguage {
    private static SharedPreferences sp;
    private static SharedPreferences.Editor editor;
    public static void init(Context context){
        sp = context.getSharedPreferences(CommonConst.SP_LANGUAGE,
                context.MODE_PRIVATE);
        editor = sp.edit();
    }
    /**
     * 读取语言设置
     * @return 语言简称
     */
    public static String getLanguage(){
        return sp.getString(CommonConst.SP_LANGUAGE, "");
    }

    /**
     * 设置语言
     * @param lang 语言简称
     */
    public static void setLanguage(String lang){
        editor.putString(CommonConst.SP_LANGUAGE, lang);
        editor.apply();
    }

    /**
     * 读取语言设置
     * @return 语言简称
     */
    public static String getCurrentLanguage(){
        return sp.getString(CommonConst.SP_CURRENT_LANGUAGE, "");
    }

    /**
     * 设置语言
     * @param lang 语言简称
     */
    public static void setCurrentLanguage(String lang){
        editor.putString(CommonConst.SP_CURRENT_LANGUAGE, lang);
        editor.apply();
    }
}
