/*
 * 版权所有 2009-2012山东新北洋信息技术股份有限公司保留所有权利。
 */


package com.easipos.easilocker.snbc.common;
/**
 * 包名称：com.snbc.bvmtest.common
 * 描述：公共变量类
 * 作者：王峰
 * 日期：2019/10/30
 * 公司：山东新北洋信息技术股份有限公司
 */
public class CommonConst {
    public static final String SP_CURRENT_LANGUAGE = "currentLanguage";
    public static final String SP_LANGUAGE = "language";
}
