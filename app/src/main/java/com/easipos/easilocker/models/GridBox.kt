package com.easipos.easilocker.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class GridBox(
    val gridId: Int,
    val boxId: Int = 1
) : Parcelable