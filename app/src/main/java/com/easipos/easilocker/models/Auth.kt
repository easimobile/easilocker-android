package com.easipos.easilocker.models

data class Auth(val token: String)