package com.easipos.easilocker.api.services

import com.easipos.easilocker.api.ApiEndpoint
import com.easipos.easilocker.api.misc.ResponseModel
import okhttp3.RequestBody
import retrofit2.http.Body
import retrofit2.http.POST

interface Api {

    @POST(ApiEndpoint.CHECK_VERSION)
    suspend fun checkVersion(@Body body: RequestBody): ResponseModel<Boolean>
}
