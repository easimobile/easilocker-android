package com.easipos.easilocker.activities.main.mvp

import android.app.Application
import com.alibaba.fastjson.JSONObject
import com.easipos.easilocker.R
import com.easipos.easilocker.snbc.common.CommonServerBind
import com.easipos.easilocker.snbc.common.CommonSharedPreLanguage
import com.easipos.easilocker.tools.Preference
import com.snbc.parambean.CommonResBean
import io.github.anderscheow.library.mvp.AbstractPresenter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class MainPresenter(private val application: Application)
    : AbstractPresenter<MainView>(application) {

    fun doLogin() {
        Preference.prefPassword.takeIf { it.isNotBlank() }?.let { password ->
            launch {
                try {
                    if (CommonServerBind.getInstance().isBindServer.not()) {
                        //重新绑定服务
                        CommonServerBind.getInstance().bindServer(application)
                        CommonSharedPreLanguage.init(application)
                    }

                    val result = CommonServerBind.getInstance().stub.init(password)
                    val commonResultBean =
                        JSONObject.parseObject(result, CommonResBean::class.java)

                    if (commonResultBean != null && commonResultBean.result == "99") {
                        withContext(Dispatchers.Main) {
                            view?.showErrorAlertDialog(application.getString(R.string.prompt_login_successfully))
                        }
                    } else {
                        withContext(Dispatchers.Main) {
                            view?.showErrorAlertDialog(application.getString(R.string.error_unknown))
                        }
                    }
                } catch (ex: Exception) {
                    ex.printStackTrace()
                    withContext(Dispatchers.Main) {
                        view?.showErrorAlertDialog(ex.message ?: "")
                    }
                }
            }
        }
    }
}
