package com.easipos.easilocker.activities.grid_control.mvp

import android.app.Application
import com.alibaba.fastjson.JSON
import com.easipos.easilocker.bvm.GridAidlInterface
import com.easipos.easilocker.models.GridBox
import com.easipos.easilocker.snbc.common.CommonServerBind
import com.easipos.easilocker.snbc.common.CommonSharedPreLanguage
import com.snbc.parambean.GridDoorReqBean
import io.github.anderscheow.library.mvp.AbstractPresenter
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext

class GridControlPresenter(private val application: Application)
    : AbstractPresenter<GridControlView>(application) {

    /*launch {
        try {

            withContext(Dispatchers.Main) {
                view?.populateOutput(result)
            }
        } catch (ex: Exception) {
            withContext(Dispatchers.Main) {
                view?.clearOutput()
                view?.showErrorAlertDialog(ex.message ?: "")
            }
        }
    }*/

    fun openGridDoor(gridBox: GridBox) {
        launch {
            try {
                val req = arrayListOf<GridDoorReqBean>().apply {
                    this.add(GridDoorReqBean(gridBox.boxId, gridBox.gridId))
                }

                val result = getStub().openCabinetDoor(JSON.toJSONString(req))

                withContext(Dispatchers.Main) {
                    view?.populateOutput(result)
                }
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    view?.clearOutput()
                    view?.showErrorAlertDialog(ex.message ?: "")
                }
            }
        }
    }

    fun queryDoorStatus(gridBox: GridBox) {
        launch {
            try {
                val req = arrayListOf<GridDoorReqBean>().apply {
                    this.add(GridDoorReqBean(gridBox.boxId, gridBox.gridId))
                }

                val result = getStub().queryCabinetStatus(JSON.toJSONString(req))

                withContext(Dispatchers.Main) {
                    view?.populateOutput(result)
                }
            } catch (ex: Exception) {
                withContext(Dispatchers.Main) {
                    view?.clearOutput()
                    view?.showErrorAlertDialog(ex.message ?: "")
                }
            }
        }
    }

    private fun getStub(): GridAidlInterface {
        if (CommonServerBind.getInstance().isBindServer.not()) {
            //重新绑定服务
            CommonServerBind.getInstance().bindServer(application)
            CommonSharedPreLanguage.init(application)
        }
        return CommonServerBind.getInstance().stub
    }
}
