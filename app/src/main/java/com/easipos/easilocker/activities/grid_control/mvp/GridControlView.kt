package com.easipos.easilocker.activities.grid_control.mvp

import io.github.anderscheow.library.mvp.MvpView

interface GridControlView : MvpView {

    fun populateOutput(output: String)

    fun clearOutput()
}
