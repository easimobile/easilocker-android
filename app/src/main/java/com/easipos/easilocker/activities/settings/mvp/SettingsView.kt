package com.easipos.easilocker.activities.settings.mvp

import io.github.anderscheow.library.mvp.MvpView

interface SettingsView : MvpView {

    fun finishScreen()
}
