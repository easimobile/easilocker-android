package com.easipos.easilocker.activities.main

import android.content.Context
import android.content.Intent
import android.os.Bundle
import androidx.recyclerview.widget.GridLayoutManager
import com.easipos.easilocker.R
import com.easipos.easilocker.activities.main.mvp.MainPresenter
import com.easipos.easilocker.activities.main.mvp.MainView
import com.easipos.easilocker.activities.main.navigation.MainNavigation
import com.easipos.easilocker.adapters.GridBoxAdapter
import com.easipos.easilocker.bundle.RequestCode
import com.easipos.easilocker.models.GridBox
import com.easipos.easilocker.tools.Preference
import io.github.anderscheow.library.appCompat.activity.MvpBaseActivity
import io.github.anderscheow.library.kotlinExt.click
import kotlinx.android.synthetic.main.activity_main.*
import org.jetbrains.anko.longToast
import org.kodein.di.instance

class MainActivity : MvpBaseActivity<MainView, MainPresenter>(), GridBoxAdapter.OnGestureDetectedListener {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, MainActivity::class.java)
        }
    }

    //region Variables
    private val navigation by instance<MainNavigation>()

    private val mainPresenter by lazy { MainPresenter(application) }
    //endregion

    //region Lifecycle
    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)
        if (requestCode == RequestCode.RC_SETTINGS && resultCode == RESULT_OK) {
            setupRecyclerView()
        }
    }
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_main

    override fun getMvpView(): MainView {
        return object : MainView {
            override fun setLoadingIndicator(active: Boolean, message: Int) {
                checkLoadingIndicator(active, message)
            }

            override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
                showYesAlertDialog(message, title, R.string.action_ok, action = action)
            }

            override fun toastMessage(message: CharSequence) {
                longToast(message)
            }

            override fun toastMessage(message: Int) {
                longToast(message)
            }
        }
    }

    override fun getPresenter(): MainPresenter {
        return mainPresenter
    }

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)

        setupViews()
        setupListeners()

        getPresenter().doLogin()
    }
    //endregion

    //region Interface Methods
    override fun onSelectItem(item: GridBox) {
        navigation.navigateToGridControl(this, item)
    }
    //endregion

    //region Action Methods
    private fun setupViews() {
        setupRecyclerView()
    }

    private fun setupListeners() {
        image_view_settings.click {
            navigation.navigateToSettings(this)
        }
    }

    private fun setupRecyclerView() {
        val row = Preference.prefGridRow
        val column = Preference.prefGridColumn

        if (row != 0 && column != 0) {
            val items = generateGrids(row, column)

            recycler_view_grid_box.apply {
                this.layoutManager = GridLayoutManager(this@MainActivity, column)
                this.adapter = GridBoxAdapter(this@MainActivity, this@MainActivity).apply {
                    this.items = items.toMutableList()
                }
            }
        }
    }

    private fun generateGrids(row: Int, column: Int): List<GridBox> {
        val list = arrayListOf<GridBox>()

        for (i in 0 until row) {
            for (j in 0 until column) {
                list.add(GridBox(i + (row * j)))
            }
        }

        return list
    }
    //endregion
}
