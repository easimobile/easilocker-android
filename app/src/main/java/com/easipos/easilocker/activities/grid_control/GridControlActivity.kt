package com.easipos.easilocker.activities.grid_control

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.easipos.easilocker.R
import com.easipos.easilocker.activities.grid_control.mvp.GridControlPresenter
import com.easipos.easilocker.activities.grid_control.mvp.GridControlView
import com.easipos.easilocker.bundle.ParcelData
import com.easipos.easilocker.models.GridBox
import io.github.anderscheow.library.appCompat.activity.MvpBaseActivity
import io.github.anderscheow.library.kotlinExt.argument
import io.github.anderscheow.library.kotlinExt.click
import kotlinx.android.synthetic.main.activity_grid_control.*
import org.jetbrains.anko.longToast

class GridControlActivity : MvpBaseActivity<GridControlView, GridControlPresenter>(){

    companion object {
        fun newIntent(context: Context, gridBox: GridBox): Intent {
            return Intent(context, GridControlActivity::class.java).apply {
                this.putExtra(ParcelData.GRID_BOX, gridBox)
            }
        }
    }

    //region Variables
    private val gridControlPresenter by lazy { GridControlPresenter(application) }

    private val gridBox by argument<GridBox>(ParcelData.GRID_BOX)
    //endregion

    //region Lifecycle

    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_grid_control

    override fun getMvpView(): GridControlView {
        return object : GridControlView {
            override fun setLoadingIndicator(active: Boolean, message: Int) {
                checkLoadingIndicator(active, message)
            }

            override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
                showYesAlertDialog(message, title, R.string.action_ok, action = action)
            }

            override fun toastMessage(message: CharSequence) {
                longToast(message)
            }

            override fun toastMessage(message: Int) {
                longToast(message)
            }

            override fun populateOutput(output: String) {
                text_view_output.text = output
            }

            override fun clearOutput() {
                text_view_output.text = null
            }
        }
    }

    override fun getPresenter(): GridControlPresenter {
        return gridControlPresenter
    }

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)

        setupViews()
        setupListeners()
    }
    //endregion

    //region Interface Methods
    //endregion

    //region Action Methods
    private fun setupViews() {
        gridBox?.let { gridBox ->
            text_view_grid_id.text = gridBox.gridId.toString()
        }
    }

    private fun setupListeners() {
        image_view_back.click {
            finish()
        }

        gridBox?.let { gridBox ->
            card_view_open_door.click {
                getPresenter().openGridDoor(gridBox)
            }

            card_view_query_status_status.click {
                getPresenter().queryDoorStatus(gridBox)
            }

            card_view_open_grid_disinfection.click {

            }

            card_view_close_grid_disinfection.click {

            }

            card_view_query_grid_disinfection_status.click {

            }

            view_colour_red.click {

            }

            view_colour_white.click {

            }

            view_colour_yellow.click {

            }

            view_colour_blue.click {

            }

            view_colour_green.click {

            }

            switch_grid_light.setOnCheckedChangeListener { _, isChecked ->

            }

            switch_grid_twinkle.setOnCheckedChangeListener { _, isChecked ->

            }

            switch_box_light.setOnCheckedChangeListener { _, isChecked ->

            }

            switch_box_type.setOnCheckedChangeListener { _, isChecked ->

            }
        }
    }
    //endregion
}
