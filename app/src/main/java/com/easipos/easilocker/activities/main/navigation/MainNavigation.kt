package com.easipos.easilocker.activities.main.navigation

import android.app.Activity
import com.easipos.easilocker.models.GridBox

interface MainNavigation {

    fun navigateToSettings(activity: Activity)

    fun navigateToGridControl(activity: Activity, gridBox: GridBox)
}