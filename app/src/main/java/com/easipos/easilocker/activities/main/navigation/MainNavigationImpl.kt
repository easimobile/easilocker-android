package com.easipos.easilocker.activities.main.navigation

import android.app.Activity
import com.easipos.easilocker.activities.grid_control.GridControlActivity
import com.easipos.easilocker.activities.settings.SettingsActivity
import com.easipos.easilocker.bundle.RequestCode
import com.easipos.easilocker.models.GridBox

class MainNavigationImpl : MainNavigation {

    override fun navigateToSettings(activity: Activity) {
        activity.startActivityForResult(SettingsActivity.newIntent(activity), RequestCode.RC_SETTINGS)
    }

    override fun navigateToGridControl(activity: Activity, gridBox: GridBox) {
        activity.startActivity(GridControlActivity.newIntent(activity, gridBox))
    }
}