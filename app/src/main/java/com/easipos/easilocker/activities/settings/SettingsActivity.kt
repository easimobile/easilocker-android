package com.easipos.easilocker.activities.settings

import android.content.Context
import android.content.Intent
import android.os.Bundle
import com.easipos.easilocker.BuildConfig
import com.easipos.easilocker.R
import com.easipos.easilocker.activities.settings.mvp.SettingsPresenter
import com.easipos.easilocker.activities.settings.mvp.SettingsView
import com.easipos.easilocker.tools.Preference
import io.github.anderscheow.library.appCompat.activity.MvpBaseActivity
import io.github.anderscheow.library.kotlinExt.click
import io.github.anderscheow.validator.Validator
import io.github.anderscheow.validator.extensions.validate
import io.github.anderscheow.validator.rules.common.notBlank
import io.github.anderscheow.validator.rules.regex.digitsOnly
import kotlinx.android.synthetic.main.activity_settings.*
import org.jetbrains.anko.longToast
import org.kodein.di.instance

class SettingsActivity : MvpBaseActivity<SettingsView, SettingsPresenter>() {

    companion object {
        fun newIntent(context: Context): Intent {
            return Intent(context, SettingsActivity::class.java)
        }
    }

    //region Variables
    private val validator by instance<Validator>()

    private val settingsPresenter by lazy { SettingsPresenter(application) }
    //endregion

    //region Lifecycle
    //endregion

    //region CustomBaseAppCompatActivity Abstract Methods
    override fun getResLayout(): Int = R.layout.activity_settings

    override fun getMvpView(): SettingsView {
        return object : SettingsView {
            override fun setLoadingIndicator(active: Boolean, message: Int) {
                runOnUiThread {
                    checkLoadingIndicator(active, message)
                }
            }

            override fun showErrorAlertDialog(message: CharSequence, title: CharSequence?, action: () -> Unit) {
                runOnUiThread {
                    showYesAlertDialog(message, title, R.string.action_ok, action = action)
                }
            }

            override fun toastMessage(message: CharSequence) {
                runOnUiThread {
                    longToast(message)
                }
            }

            override fun toastMessage(message: Int) {
                runOnUiThread {
                    longToast(message)
                }
            }

            override fun finishScreen() {
                runOnUiThread {
                    setResult(RESULT_OK)
                    finish()
                }
            }
        }
    }

    override fun getPresenter(): SettingsPresenter {
        return settingsPresenter
    }

    override fun init(savedInstanceState: Bundle?) {
        super.init(savedInstanceState)

        setupViews()
        setupListeners()
    }
    //endregion

    //region Action Methods
    private fun setupViews() {
        text_input_edit_text_password.setText(Preference.prefPassword)
        text_input_edit_text_row.setText(Preference.prefGridRow.toString())
        text_input_edit_text_column.setText(Preference.prefGridColumn.toString())
        text_view_version.text = getString(R.string.label_version, BuildConfig.VERSION_NAME)
    }

    private fun setupListeners() {
        image_view_back.click {
            finish()
        }

        image_view_save.click {
            attemptSaveSettings()
        }
    }

    private fun attemptSaveSettings() {
        val passwordValidation = text_input_layout_password.validate()
            .notBlank(R.string.error_field_required)
        val rowValidation = text_input_layout_row.validate()
            .notBlank(R.string.error_field_required)
            .digitsOnly()
        val columnValidation = text_input_layout_column.validate()
            .notBlank(R.string.error_field_required)
            .digitsOnly()

        validator.setListener(object : Validator.OnValidateListener {
            override fun onValidateFailed(errors: List<String>) {
            }

            override fun onValidateSuccess(values: List<String>) {
                val password = values[0].trim()
                val row = values[1].trim().toIntOrNull() ?: 0
                val column = values[2].trim().toIntOrNull() ?: 0

                Preference.prefPassword = password
                Preference.prefGridRow = row
                Preference.prefGridColumn = column

                getPresenter().doLogin(password)
            }
        }).validate(passwordValidation, rowValidation, columnValidation)
    }
    //endregion
}
