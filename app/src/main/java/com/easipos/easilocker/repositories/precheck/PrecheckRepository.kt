package com.easipos.easilocker.repositories.precheck

import com.easipos.easilocker.api.requests.precheck.CheckVersionRequestModel
import com.easipos.easilocker.models.Result

interface PrecheckRepository {

    suspend fun checkVersion(model: CheckVersionRequestModel): Result<Boolean>
}
