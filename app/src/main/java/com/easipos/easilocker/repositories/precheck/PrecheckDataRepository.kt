package com.easipos.easilocker.repositories.precheck

import com.easipos.easilocker.api.requests.precheck.CheckVersionRequestModel
import com.easipos.easilocker.datasource.DataFactory
import com.easipos.easilocker.models.Result

class PrecheckDataRepository(private val dataFactory: DataFactory) : PrecheckRepository {

    override suspend fun checkVersion(model: CheckVersionRequestModel): Result<Boolean> =
        dataFactory.createPrecheckDataSource()
            .checkVersion(model)
}
