package com.easipos.easilocker.adapters

import android.content.Context
import android.view.View
import android.view.ViewGroup
import androidx.databinding.DataBindingUtil
import androidx.recyclerview.widget.RecyclerView
import com.easipos.easilocker.R
import com.easipos.easilocker.databinding.ViewGridBoxBinding
import com.easipos.easilocker.models.GridBox
import io.github.anderscheow.library.kotlinExt.invisible
import io.github.anderscheow.library.kotlinExt.visible
import io.github.anderscheow.library.recyclerView.adapters.BaseRecyclerViewAdapter
import io.github.anderscheow.library.recyclerView.viewHolder.BaseViewHolder
import kotlinx.android.extensions.LayoutContainer
import kotlinx.android.synthetic.main.view_grid_box.*

class GridBoxAdapter(context: Context,
                     private val listener: OnGestureDetectedListener)
    : BaseRecyclerViewAdapter<GridBox>(context) {

    interface OnGestureDetectedListener {
        fun onSelectItem(item: GridBox)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        val binding = DataBindingUtil.inflate<ViewGridBoxBinding>(
            layoutInflater, R.layout.view_grid_box, parent, false)
        return GridBoxViewHolder(binding)
    }

    override fun onBindViewHolder(holder: RecyclerView.ViewHolder, position: Int) {
        if (holder is GridBoxViewHolder) {
            holder.bind(items[holder.adapterPosition])
        }
    }

    inner class GridBoxViewHolder(private val binding: ViewGridBoxBinding)
        : BaseViewHolder<GridBox>(binding), LayoutContainer {

        override val containerView: View
            get() = binding.root

        override fun extraBinding(item: GridBox) {
            if (adapterPosition == 0) {
                layout_root.invisible()
            } else {
                layout_root.visible()
            }
        }

        override fun onClick(view: View, item: GridBox?) {
            item?.let {
                listener.onSelectItem(item)
            }
        }
    }
}