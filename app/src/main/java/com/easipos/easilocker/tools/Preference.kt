package com.easipos.easilocker.tools

import com.pixplicity.easyprefs.library.Prefs

object Preference {

    private const val PREF_LANGUAGE_CODE = "PREF_LANGUAGE_CODE"
    private const val PREF_ACCESS_TOKEN = "PREF_ACCESS_TOKEN"
    private const val PREF_IS_LOGGED_IN = "PREF_IS_LOGGED_IN"
    private const val PREF_PASSWORD = "PREF_PASSWORD"
    private const val PREF_GRID_ROW = "PREF_GRID_ROW"
    private const val PREF_GRID_COLUMN = "PREF_GRID_COLUMN"

    var prefLanguageCode: String
        get() = Prefs.getString(PREF_LANGUAGE_CODE, "en")
        set(languageCode) = Prefs.putString(PREF_LANGUAGE_CODE, languageCode)

    var prefAccessToken: String
        get() = Prefs.getString(PREF_ACCESS_TOKEN, "")
        set(accessToken) = Prefs.putString(PREF_ACCESS_TOKEN, accessToken)

    var prefIsLoggedIn: Boolean
        get() = Prefs.getBoolean(PREF_IS_LOGGED_IN, false)
        set(isLoggedIn) = Prefs.putBoolean(PREF_IS_LOGGED_IN, isLoggedIn)

    var prefPassword: String
        get() = Prefs.getString(PREF_PASSWORD, "")
        set(password) = Prefs.putString(PREF_PASSWORD, password)

    var prefGridRow: Int
        get() = Prefs.getInt(PREF_GRID_ROW, 0)
        set(row) = Prefs.putInt(PREF_GRID_ROW, row)

    var prefGridColumn: Int
        get() = Prefs.getInt(PREF_GRID_COLUMN, 0)
        set(column) = Prefs.putInt(PREF_GRID_COLUMN, column)

    fun logout() {
        prefIsLoggedIn = false

        Prefs.remove(PREF_ACCESS_TOKEN)
    }
}