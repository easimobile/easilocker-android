package com.easipos.easilocker.di.modules

import com.easipos.easilocker.activities.main.navigation.MainNavigation
import com.easipos.easilocker.activities.main.navigation.MainNavigationImpl
import io.github.anderscheow.library.di.modules.ActivityBaseModule
import org.kodein.di.DI
import org.kodein.di.bind
import org.kodein.di.provider

class ActivityModule : ActivityBaseModule() {
    override fun provideAdditionalModule(builder: DI.Builder) {
        builder.apply {
            bind<MainNavigation>() with provider { MainNavigationImpl() }
        }
    }
}
