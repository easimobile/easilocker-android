package com.easipos.easilocker.event_bus

data class NotificationCount(val count: Int)