package com.easipos.easilocker.bundle

object ParcelData {

    const val CLEAR_DB = "clear_db"
    const val URL = "url"
    const val GRID_BOX = "GRID_BOX"
}