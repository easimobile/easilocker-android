package com.easipos.easilocker.datasource

import com.easipos.easilocker.api.services.Api
import com.easipos.easilocker.datasource.precheck.PrecheckDataSource
import com.easipos.easilocker.datasource.precheck.PrecheckDataStore

class DataFactory(
    private val api: Api
) {

    fun createPrecheckDataSource(): PrecheckDataStore =
        PrecheckDataSource(api)
}
