package com.easipos.easilocker.datasource.precheck

import com.easipos.easilocker.api.requests.precheck.CheckVersionRequestModel
import com.easipos.easilocker.models.Result

interface PrecheckDataStore {

    suspend fun checkVersion(model: CheckVersionRequestModel): Result<Boolean>
}
