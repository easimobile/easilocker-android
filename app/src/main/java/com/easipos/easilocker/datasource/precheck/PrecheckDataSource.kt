package com.easipos.easilocker.datasource.precheck

import com.easipos.easilocker.api.misc.parseException
import com.easipos.easilocker.api.misc.parseResponse
import com.easipos.easilocker.api.requests.precheck.CheckVersionRequestModel
import com.easipos.easilocker.api.services.Api
import com.easipos.easilocker.models.Result
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext

class PrecheckDataSource(private val api: Api) : PrecheckDataStore {

    override suspend fun checkVersion(model: CheckVersionRequestModel): Result<Boolean> {
        return withContext(Dispatchers.IO) {
            try {
                val response = api.checkVersion(model.toFormDataBuilder().build())
                parseResponse(response) {
                    it
                }
            } catch (ex: Exception) {
                parseException(ex)
            }
        }
    }
}
