// GridAidlInterface.aidl
package com.easipos.easilocker.bvm;

// Declare any non-default types here with import statements

interface GridAidlInterface {

        String init(String key);

        String getFactoryCode(String param);

        String updateFirmware(String param);

        String getMachineInfo(String param);

        String openCabinetDoor(String param);

        String queryCabinetStatus(String param);

        String openGridDoor(String param);

        String queryGridStatus(String param);

        String ctrlGridLight(String param);

        String ctrlCabinetLight(String param);

        String ctrlGridHeating (String param);

        String getGridHeatingStatus (String param);

        String ctrlGridDisinfection (String param);

        String getGridDisinfectionStatus (String para);

}